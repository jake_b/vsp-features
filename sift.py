import cv2
import numpy as np

# N.B. OpenCV 3 doens't include SIFT anymore
# pip install opencv-contrib-python

"""		
img = cv2.imread('face2.jpg')
gray= cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
"""

VIDEO_FILE_P = "data\\01M\\straightcam"
VIDEO_FILE_TYPE = "*.mp4"

output = open('output_hog.csv', 'w') # ab
os.chdir(VIDEO_FILE_P)
outwriter = csv.writer(output, delimiter=',', quotechar='\"',  quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
outwriter.writerow(["file", "frame_number", "feature_vector"])

fcount = 0
for file in glob.glob(VIDEO_FILE_TYPE):
	frame_n = 0
	fcount += 1
	# print(file)
	print("File %d/%d" % (fcount, len(glob.glob(VIDEO_FILE_TYPE))), end="\r")
	cap = cv2.VideoCapture(file)
	
	while (True):
		#frame = cv2.imread('face.jpg', cv2.IMREAD_GRAYSCALE)
		ret, frame = cap.read()
		if not ret: # End of File
			break
		frame_n += 1
		#cv2.imshow('window-name', frame)
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

		sift = cv2.xfeatures2d.SIFT_create()
		kp, des = sift.detectAndCompute(gray, None)

kp_img=cv2.drawKeypoints(gray,kp,img,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

print("Feature Vector: ", des)

"""
cv2.imshow("Image", kp_img)
cv2.waitKey(0)
cv2.destroyAllWindows()
"""