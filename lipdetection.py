import numpy as np  
import cv2  
import dlib  
import sys

PADDING = 20

def rect_to_bb(rect):
	# take a bounding predicted by dlib and convert it
	# to the format (x, y, w, h) as we would normally do
	# with OpenCV
	x = rect.left()
	y = rect.top()
	w = rect.right() - x
	h = rect.bottom() - y
 
	# return a tuple of (x, y, w, h)
	return (x, y, w, h)
	
def shape_to_np(shape, dtype="int"):
	# initialize the list of (x, y)-coordinates
	coords = np.zeros((68, 2), dtype=dtype)
 
	# loop over the 68 facial landmarks and convert them
	# to a 2-tuple of (x, y)-coordinates
	for i in range(0, 68):
		coords[i] = (shape.part(i).x, shape.part(i).y)
 
	# return the list of (x, y)-coordinates
	return coords
	

PREDICTOR_PATH = "shape_predictor_68_face_landmarks.dat"  

MOUTH_OUTLINE_POINTS = list(range(48, 61))  
MOUTH_INNER_POINTS = list(range(61, 68))  
	
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(PREDICTOR_PATH)  

def im2gry(image):
	return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 
	
def get_first_face(gray):
	rects = detector(gray, 1)
	return rects[0]

def roi_dims(gray, rect, padding=5):
	landmarks = np.matrix([[p.x, p.y] for p in predictor(gray, rect).parts()])  
	   
	#landmarks_display = landmarks[MOUTH_OUTLINE_POINTS + MOUTH_INNER_POINTS]
	landmarks_display = landmarks[MOUTH_OUTLINE_POINTS] 
	
	max_x = max_y = min_x = min_y = None
	for idx, point in enumerate(landmarks_display):
		pos = (point[0, 0], point[0, 1])
		#print("Position: ", pos)	
		if max_x == None or pos[0] > max_x:
			max_x = pos[0]
		if max_y == None or pos[1] > max_y:
			max_y = pos[1]
		if min_x == None or pos[0] < min_x:
			min_x = pos[0] 
		if min_y == None or pos[1] < min_y:
			min_y = pos[1] 
		
	min_x -= padding
	min_y -= padding
	max_x += padding
	max_y += padding
		
	return (min_x, min_y, max_x, max_y)
		
def e_roi(gray, dims):
	min_x, min_y, max_x, max_y = dims
	return gray[min_y:max_y, min_x:max_x]

def rect2min_n_max(rect):
	x,y,w,h = rect_to_bb(rect)
	min_x = x
	min_y = y
	max_x = x+w
	max_y = y+h
	return (min_x, min_y, max_x, max_y)
	
def bounding_square(dims):
	min_x, min_y, max_x, max_y = dims
	width = (max_x - min_x)
	height = (max_y - min_y)
	
	center_x = int(min_x + (width/2))
	center_y = int(min_y + (height/2))
	
	new_size = int(width/2) if width < height else int(height/2)
	nmax_x = center_x + new_size
	nmax_y = center_y + new_size
	
	nmin_x = center_x - new_size
	nmin_y = center_y - new_size
	
	return (nmin_x, nmin_y, nmax_x, nmax_y)
	
def extract_face_image(rect, gray, std_size=512):
	min_x, min_y, max_x, max_y = rect2min_n_max(rect)
	face = gray[min_y:max_y, min_x:max_x]
	return cv2.resize(face, (std_size, std_size))
	
if __name__ == "__main__":
	imagePath = "face.jpg"

	# Read the image  
	image = cv2.imread(imagePath) 
	gray = im2gry(image) 

	# Detect faces in the image  
	rect = get_first_face(gray)
	face = extract_face_image(rect, gray)
	gray = np.copy(face)
	
	# cv2.imshow("Face", face)
	rect = get_first_face(face)
	# If the face are isn't square, get the square at the center of the bounding rectangle (unlikely since the method returns a square)
	#min_x, min_y, max_x, max_y = bounding_square((min_x, min_y, max_x, max_y))
	#face2 = gray[min_y:max_y, min_x:max_x]
	#print(face2.shape)
	
	landmarks = np.matrix([[p.x, p.y] for p in predictor(gray, rect).parts()])  
   
	#landmarks_display = landmarks[MOUTH_OUTLINE_POINTS + MOUTH_INNER_POINTS]
	landmarks_display = landmarks[MOUTH_OUTLINE_POINTS] 
	
	for idx, point in enumerate(landmarks_display):
		# Draw in Gray not the Face!
		pos = (point[0, 0], point[0, 1])
		cv2.circle(gray, pos, 2, color=(0, 255, 255), thickness=-1)
		
		min_x, min_y, max_x, max_y = roi_dims(gray, rect)
		
		cv2.rectangle(gray, (min_x, min_y), (max_x, max_y), (0, 0, 0), 2)
		roi = e_roi(face, (min_x, min_y, max_x, max_y))
		
		min_x, min_y, max_x, max_y = rect2min_n_max(rect)
		cv2.rectangle(gray, (min_x, min_y), (max_x, max_y), (255, 0, 0), 2)
		
		
	# show the output image with the face detections + facial landmarks
	#cv2.imshow("Output", image)
	cv2.imshow("ROI", roi)
	cv2.imshow("Image", gray)
	cv2.waitKey(0)