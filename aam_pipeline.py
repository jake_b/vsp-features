# %matplotlib inline
# pip install menpo, menpofit, menpowidgets
import numpy as np
from menpowidgets import visualize_aam, visualize_images, visualize_appearance_model # {?} visualize_shape_model
# {? } from menpo.visualize import visualize_shapes
import menpo.io as mio
from menpo.landmark import labeller
from menpo.landmark import face_ibug_68_to_face_ibug_68_trimesh as ibug_face_66
from menpofit.aam import HolisticAAM, LucasKanadeAAMFitter, WibergInverseCompositional
from menpo.feature import hog as mphog
from menpodetect import load_dlib_frontal_face_detector
import os, glob
import dlib
import menpo

# Get a set of images to train on:
# ffmpeg -i sa1.mp4 -vf fps=2 train/out%d.png
# where fps is the number of seconds between output frames

DATA_PATH = "data\\01M\\"
TRAINING_SET_PATH = DATA_PATH + "train"
dlib_detector = load_dlib_frontal_face_detector()

detector = dlib.get_frontal_face_detector()

PREDICTOR_PATH = "shape_predictor_68_face_landmarks.dat"  

MOUTH_OUTLINE_POINTS = list(range(48, 61))  
MOUTH_INNER_POINTS = list(range(61, 68))  
predictor = dlib.shape_predictor(PREDICTOR_PATH) 

images = []
for i in mio.import_images(TRAINING_SET_PATH,  max_images=10, verbose=True):
	if i.n_channels == 3:
		i = i.as_greyscale(mode='luminosity')
	
	dlib_detector(i)
	i = i.crop_to_landmarks_proportion(0.5, 'dlib_0')
	i = i.rescale_landmarks_to_diagonal_range(100, 'dlib_0')
	
	rect = detector(i.as_imageio(), 1)[0]
	
	landmarks = np.matrix([[p.y, p.x] for p in predictor(i.as_imageio(), rect).parts()])
	landmarks_pc = menpo.shape.PointCloud(landmarks)
	lm_lbl = menpo.landmark.face_ibug_68_to_face_ibug_68(landmarks_pc)
	
	i.landmarks['PTS'] = lm_lbl
	#print(landmarks)
	
	images.append(i)
	
#visualize_images(images)
#sys.exit()

# visualize_images(images)
aam = HolisticAAM(images, diagonal=150,
                  scales=1, group='PTS', holistic_features=mphog, verbose=True,
                  max_shape_components=20, max_appearance_components=150)  
print(aam)
	  
fitter = LucasKanadeAAMFitter(aam, lk_algorithm_cls=WibergInverseCompositional, n_shape=5, n_appearance=30)
						  
print(fitter)		

VIDEO_FILE_TYPE = "*.mp4"
fcount = 0

frame_count = 0
for video in mio.import_videos(DATA_PATH + VIDEO_FILE_TYPE):
	frames = []
	for image in video:
		frame_count += 1
		image = image.as_greyscale()
		image = image.rescale(0.2)
		# Face detection
		detect = load_dlib_frontal_face_detector()
		bboxes = detect(image)

		# Fit AAM to Test Image
		initial_bbox = bboxes[0]
		result = fitter.fit_from_bb(image, initial_bbox, max_iters=8)
		# print("Result: ", result.shapes)
		# result.view_iterations()
		# result.view(render_initial_shape=True)
		
		# Get the lip landmark positions.
		print(result)
		
		# print("Length: ", len(result.final_shape.lms))
		"""
		print(result.shape_parameters[-1])
		print(result.appearance_parameters[-1])
		"""