import cv2
import scipy.io as io
import numpy as np
from lipdetection import *
from pprint import pprint
import glob, os, csv
from dct import select_zz_mask, N_COEFFS
from skimage.feature import hog
from skimage import data, color, exposure
import pickle # protocol 0 is printable ASCII

#serialized = pickle.dumps(a, protocol=0) 
#deserialized_a = pickle.loads(serialized)

std_dim = 48

VIDEO_FILE_P = "data\\07"
VIDEO_FILE_TYPE = "*.mat"

output_dct = open('output_dct.csv', 'w')
output_hog = open('output_hog.csv', 'w')
output_sift = open('output_sift.csv', 'w')

os.chdir(VIDEO_FILE_P)
o_dct_wr = csv.writer(output_dct, delimiter=',', quotechar='\"',  quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
o_hog_wr = csv.writer(output_hog, delimiter=',', quotechar='\"',  quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
o_sift_wr = csv.writer(output_sift, delimiter=',', quotechar='\"',  quoting=csv.QUOTE_MINIMAL, lineterminator='\n')

o_dct_wr.writerow(["file", "frame_number", "feature_vector"])
o_hog_wr.writerow(["file", "frame_number", "feature_vector"])
o_sift_wr.writerow(["file", "frame_number", "feature_vector"])

fcount = 0
for file in glob.glob(VIDEO_FILE_TYPE):
	frame_n = 0
	fcount += 1
	# print(file)
	print("File %d/%d" % (fcount, len(glob.glob(VIDEO_FILE_TYPE))), end="\r")
	mat = io.loadmat(file)
	images = mat["ROIs"][0][0][0]

	for idx in range(len(images)):
		img = images[idx]
		img_sm = cv2.resize(img, (std_dim, std_dim),  interpolation=cv2.INTER_CUBIC)
		images[idx] = img_sm
	
	for img in images:
		frame_n += 1
		
		# 1. DCT
		dst = cv2.dct(img)           # the dct
		mask = select_zz_mask(N_COEFFS, dst.shape)
		fvec = np.extract(mask, dst).tolist()
		
		o_dct_wr.writerow([file, frame_n, pickle.dumps(fvec, protocol=0)])
		# print("Feature Vector: ", fvec) 
		
		# 2. HoG
		hog_v = hog(img, orientations=9, pixels_per_cell=(8, 8), cells_per_block=(1, 1), block_norm="L2-Hys", visualise=False, transform_sqrt=False, feature_vector=True)
		
		hog_v_serialized = pickle.dumps(hog_v, protocol=0) 
		#print("Feature Vector: ", hog_v)
		o_hog_wr.writerow([file, frame_n, hog_v_serialized])
		
		# 3. SIFT
		sift = cv2.xfeatures2d.SIFT_create()
		img8 = (img*255).astype('uint8')
		kp, des = sift.detectAndCompute(img8, None)
		#print("Descriptor: ", des)
		o_sift_wr.writerow([file, frame_n, pickle.dumps(des, protocol=0)])
	
	
