from lipdetection import predictor, get_first_face, extract_face_image
import cv2
import numpy as np
import os, json, glob

DATA_PATH = "data\\01M"
TRAINING_SET_PATH = DATA_PATH + "\\train"
os.system("mkdir " + TRAINING_SET_PATH)
os.system("ffmpeg -i " + DATA_PATH + "\\sa1.mp4" + " -vf fps=2 " + DATA_PATH + "\\train\out%d.png")

os.chdir(TRAINING_SET_PATH)
for file in glob.glob("*.png"):
	f = open(str(file).replace("png", "pts"), "w", newline='')
	f.write("version: 1\n")
	f.write("n_points: 68\n")
	f.write("{\n")

	gray = cv2.imread(file, cv2.IMREAD_GRAYSCALE)
	rect = get_first_face(gray)
	face = extract_face_image(rect, gray)
	cv2.imwrite(str(file), face)
	result = predictor(gray, rect).parts()

	for p in result:
		f.write(str(p.x) + " " + str(p.y) + "\n")

	f.write("}\n")