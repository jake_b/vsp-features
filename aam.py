# %matplotlib inline
# pip install menpo, menpofit, menpowidgets
import numpy as np
from menpowidgets import visualize_aam, visualize_images, visualize_appearance_model # {?} visualize_shape_model
# {? } from menpo.visualize import visualize_shapes
import menpo.io as mio
from menpo.landmark import labeller
from menpo.landmark import face_ibug_68_to_face_ibug_68_trimesh as ibug_face_66
from menpofit.aam import HolisticAAM, LucasKanadeAAMFitter, WibergInverseCompositional
from menpo.feature import hog as mphog
from menpodetect import load_dlib_frontal_face_detector

# Get a set of images to train on:
# ffmpeg -i sa1.mp4 -vf fps=2 train/out%d.png
# where fps is the number of seconds between output frames

# Annotate the Images then set the path here.
TRAINING_SET_PATH = "lfpw\\trainset\\"
TEST_IMAGE = "lfpw\\testset\\image_0001.png"
#Users\\j\\Documents\\WS18\\VSP\\features\\
images = []
for i  in mio.import_images(TRAINING_SET_PATH,  max_images=10, verbose=False):
    
    i = i.crop_to_landmarks_proportion(0.5)
    i = i.rescale_landmarks_to_diagonal_range(100)
    labeller(i, 'PTS', ibug_face_66)
    if i.n_channels == 3:
        i = i.as_greyscale(mode='luminosity')
    
    images.append(i)

# visualize_images(images)

aam = HolisticAAM(images, group='face_ibug_68_trimesh', diagonal=150,
                  scales=(0.5, 1.0), holistic_features=mphog, verbose=True,
                  max_shape_components=20, max_appearance_components=150)
		
"""		
aam.view_shape_models_widget()
aam.view_appearance_models_widget()
aam.view_aam_widget()
"""

fitter = LucasKanadeAAMFitter(aam, lk_algorithm_cls=WibergInverseCompositional, n_shape=[5, 20], n_appearance=[30, 150])
							  
print(fitter)		

image = mio.import_image(TEST_IMAGE)
image = image.as_greyscale()	

# Face detection
detect = load_dlib_frontal_face_detector()
bboxes = detect(image)
if len(bboxes) > 0:
    image.view_landmarks(group='dlib_0', line_colour='red', render_markers=False, line_width=4)

# Fit AAM to Test Image
initial_bbox = bboxes[0]
result = fitter.fit_from_bb(image, initial_bbox, max_iters=[15, 5], gt_shape=image.landmarks['PTS'])
print("Result: ", result.shapes)
# result.view_iterations()

print(result.shape_parameters[-1])
print(result.appearance_parameters[-1])